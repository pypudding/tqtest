# transquest tranlation quality estimation
import pandas as pd
import torch
from transquest.algo.word_level.microtransquest.run_model import MicroTransQuestModel
import hydra
from omegaconf import DictConfig
from pathlib import Path
import os

def get_estimation_word_level(filename, 
                    source_column, 
                    translation_columns, 
                    language_pair,
                    output_folder, 
                    override=False):
    """
    Generate OK/BAD tags at word level for source and translated context

    Args:
        filename (str): CSV or XLSX file
        source_column (str): source context column name
        translation_columns (str): list of translated column names
        language_pair (str): en-de or en-zh
        output_folder (str): folder path to save outputs
        override (bool, optional): If true, the output will append to the original input file.
                                     Defaults to False.
    """
    
    # read data form filename
    file_path = Path(filename).resolve()
    file_ext = file_path.suffix[1:]
    if file_ext == 'csv':
        data = pd.read_csv(filename)
    elif file_ext == 'xlsx':
        data = pd.read_excel(filename, engine='openpyxl')
    else:
        raise Exception('File type is not valid. Only accept .xlsx or .csv')
    # not every lang_pair has pretrained_model
    try:
        model = MicroTransQuestModel("xlmroberta", f"TransQuest/microtransquest-{language_pair}-wiki", labels=["OK", "BAD"], use_cuda=torch.cuda.is_available())
    except Exception as e:
        print(e)

    for target_column in translation_columns:
        source_tags = []
        target_tags = []
        for i in range(len(data)):
            source_tag, target_tag = model.predict([[data.loc[i, source_column], data.loc[i, target_column]]])
            source_tags.append(source_tag)
            target_tags.append(target_tag)

        data[f'source_tags_{target_column}_{language_pair}'] = source_tags
        data[f'target_tags_{target_column}_{language_pair}'] = target_tags
    
    if override:
        data.to_excel(f'{filename}')
    else:
        save_path = os.path.join(output_folder, f'{Path(filename).resolve().stem}_{language_pair}_word_level.xlsx')
        data.to_excel(save_path)


config_path = 'configs'
config_name = 'config.yaml'

@hydra.main(config_path=config_path, config_name=config_name)
def main_get_tq_tags(cfg: DictConfig) -> None:
    filepath = cfg.filepath
    source_column = cfg.source_column
    translation_columns = cfg.translation_columns
    lang_pair = cfg.lang_pair
    output_folder = cfg.output_folder

    get_estimation_word_level(filepath, 
                    source_column,
                    translation_columns,
                    lang_pair,
                    output_folder)


if __name__ == '__main__':
    main_get_tq_tags()
