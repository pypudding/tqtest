# transquest tranlation quality estimation
import pandas as pd
import torch
from transquest.algo.sentence_level.monotransquest.run_model import MonoTransQuestModel
from transquest.algo.sentence_level.siamesetransquest.run_model import SiameseTransQuestModel
import hydra
from omegaconf import DictConfig
from pathlib import Path
import os
import numpy as np

def get_estimation(filename, 
                    source_column, 
                    translation_columns, 
                    language_pair, 
                    model_type,
                    output_folder,
                    override=False):
    """
    get qe estimation scores for translation column(s)

    Args:
        filename (str): CSV or XLSX file
        source_column (str): source context column name
        translation_columns (list): list of translation column names
        language_pair (str): en-zh or en-de
        model_type ([type]): mono or siamese
        override (bool, optional): Defaults to False. If true, the output score will append to the
                                    original filename

    Raises:
        Exception: when a non-supported file type is provided
        Exception: when a non-supported language_pair is provided
    """
    
    # read data form filename
    file_path = Path(filename).resolve()
    file_ext = file_path.suffix[1:]
    if file_ext == 'csv':
        data = pd.read_csv(filename)
    elif file_ext == 'xlsx':
        data = pd.read_excel(filename, engine='openpyxl')
    else:
        raise Exception('File type is not valid. Only accept .xlsx or .csv')
    
    if language_pair == 'en_zh' or language_pair =='en_de':
        model = get_model_sentence(model_type, language_pair)
    else:
        raise Exception("This language pair is currently not supported")
    
    for target_column in translation_columns:
        predictions = []
        for i in range(len(data)):
            if pd.isnull(data.loc[i, target_column]):
                predictions.append(np.nan)
                continue
            else:
                if model_type == 'siamese':
                    predict = model.predict([[data.loc[i, source_column], data.loc[i, target_column]]],
                                        verbose=False)
                if model_type == 'mono':
                    predict, raw_outputs = model.predict([[data.loc[i, source_column], data.loc[i, target_column]]])
                
                predictions.append(predict)
        data[f'MT_{target_column}_{language_pair}_{model_type}'] = predictions
    
    if override:
        data.to_excel(f'{filename}')
    else:
        save_path = os.path.join(output_folder, f'{Path(filename).resolve().stem}_{model_type}_{language_pair}_sentence_level.xlsx')
        data.to_excel(save_path)


def get_model_sentence(model_type, language_pair):
    ''' get siamese/mono model with a specified language pair'''
    if model_type == 'siamese':
        model = SiameseTransQuestModel(f"TransQuest/siamesetransquest-da-{language_pair}-wiki")
    elif model_type == 'mono':
        model = MonoTransQuestModel("xlmroberta", f"TransQuest/monotransquest-da-{language_pair}-wiki", num_labels=1, use_cuda=torch.cuda.is_available())
    return model


config_path = 'configs'
config_name = 'config.yaml'

@hydra.main(config_path=config_path, config_name=config_name)
def main_get_tq_score(cfg: DictConfig) -> None:
    filepath = cfg.filepath
    source_column = cfg.source_column
    translation_columns = cfg.translation_columns
    lang_pair = cfg.lang_pair
    model_type = cfg.model_type
    output_folder = cfg.output_folder

    get_estimation(filepath, 
                    source_column,
                    translation_columns,
                    lang_pair,
                    model_type,
                    output_folder)


if __name__ == '__main__':
    main_get_tq_score()

    # source_column = 'source'
    # translation_columns = ['Google Translate Neural translation',
    #                         'Google Translate translation',
    #                         'DeepL translation',
    #                         'Microsoft Translate translation',
    #                         'Systran translation']
    # lang_pair = 'en-zh'
    # model_type = 'mono'
    # get_estimation('10sentences_trans.xlsx', 
    #                 source_column, 
    #                 translation_columns,
    #                 lang_pair,
    #                 model_type,
    #                 True)
