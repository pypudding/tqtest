
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import summary_table
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import scipy
import hydra
from omegaconf import DictConfig
from pathlib import Path
import os 


def get_correlation_plot(score_list1, score_list2, list1_name=None, list2_name=None, save_path=None):
    """
    Obtain the correlation plot for score_list1 and score_list2

    Args:
        score_list1 (pandas series): a column of numbers 
        score_list2 (pandas series): a column of numbers
        list1_name (str, optional): [column name of list1]. Defaults to None. If given, the new name will replace the old name on the correlation plot
        list2_name (str, optional): [column name of list2]. Defaults to None. If given, the new name will replace the old name on the correlation plot
        save_path (str, optional): [path to save the output]. Defaults to None. 
    """
    # score_list1, score_list2 are python list objects
    x = score_list1
    y = score_list2

    # list name
    name1 = score_list1.name
    name2 = score_list2.name
    if list1_name:
        name1 = list1_name
    if list2_name:
        name2 = list2_name


    # add a column of constant 1
    res = sm.OLS(y, sm.add_constant(x)).fit()
    conf_interval = res.conf_int()

    y1 = x * conf_interval.iloc[1, 0] + conf_interval.iloc[0, 0]
    y2 = x * conf_interval.iloc[1, 0] + conf_interval.iloc[0, 1]
    y3 = x * conf_interval.iloc[1, 1] + conf_interval.iloc[0, 0]
    y4 = x * conf_interval.iloc[1, 1] + conf_interval.iloc[0, 1]
    new_df = pd.concat([y1, y2, y3, y4], axis=1)

  
    upper_bound = new_df.max(axis=1)
    lower_bound = new_df.min(axis=1)

    st, data, columns = summary_table(res, alpha=0.05)
    # Not used for now, but will leave it as it can be useful for debugging
    # preds = pd.DataFrame.from_records(data, columns=[s.replace('\n', ' ') for s in columns])
    fig = px.scatter(x=x, y=y, 
                    marginal_x='histogram', marginal_y='histogram', 
                    trendline='ols',
                    template='seaborn',
                    opacity=0.7,
                    trendline_color_override='black'
                    )

    trace_upper = go.Scatter({
        'mode' : 'lines',
        'x' : x,
        'y' : upper_bound,
        'name' : 'Upper 95% CI',
        'showlegend' : True,
        'line' : {
            'color' : 'grey'
        },
        'fill': None
    })

    trace_lower = go.Scatter( {
        'type' : 'scatter',
        'mode' : 'lines',
        'x' : x,
        'y' : lower_bound,
        'name' : 'Lower 95% CI',
        # 'fill' : 'tonexty',
        'line' : {
            'color' : 'grey'
        },
        'fillcolor' : 'rgba(176, 176, 176, 0.5)'
    })

    fig.add_trace(trace_upper)
    fig.add_trace(trace_lower)

    # get pearson correlation
    pcc, pv = scipy.stats.pearsonr(x, y)

    fig.add_annotation(
        text='Pearson r = {pcc:.2f}<br>p-value = {pv:.2e}',
        xref='paper', yref='paper',
        x=0.05, y = 0.7,
        showarrow=False,
        font=dict(size=18)
    )

    fig.update_layout(
        title = 'Correlation Test',
        xaxis_title = name1,
        yaxis_title = name2,
        font=dict(size=18),
    )

    if not save_path:
        fig.write_html('output.html')
    else:
        fig.write_html(save_path)



def get_multiple_correlation_plot(filename, col_list_1, col_list_2, save_folder):
    """
    generate multiple correlation plots given multiple pairs of columns

    Args:
        filename (dataframe): data source contains multiple columns
        col_list_1 (list): list of columns 1 e.g (pair1_A, pair2_A, pair3_A)
        col_list_2 (list): list of columns 2 e.g (pair1_B, pair2_B, pair3_B)
        save_folder (str): folder to save outputs.
    Raises:
        Exception: the length of col_list_1 and col_list_2 should be the same as they are 1-1 match
    """
    if len(col_list_1) != len(col_list_2):
        raise Exception('The length of col_list_1 must equal to the length of col_list_2')
    df = pd.read_excel(filename, engine='openpyxl')
    for i in range(len(col_list_1)):
        get_correlation_plot(df[col_list_1[i]], df[col_list_2[i]],
                                # col_list_1[i], col_list_2[i],
                                save_path=os.path.join(save_folder, f'{col_list_1[i]}_vs_{col_list_2[i]}.html'))


config_path = 'configs'
config_name = 'correlation_plot.yaml'

@hydra.main(config_path=config_path, config_name=config_name)
def main_get_correlation_plot(cfg:DictConfig) -> None:

    get_multiple_correlation_plot(Path(cfg.file_path).resolve(), cfg.col_list_1, cfg.col_list_2, cfg.save_folder)



if __name__ == '__main__':
    main_get_correlation_plot()
   
    # will move the following to docstring for demonstrating an use example
    # from numpy.random import randn
    # import numpy as np
    # x1 = pd.Series(20 * randn(1000) + 100, name="list1")
    # y1 = pd.Series(x1 + (10 * randn(1000) + 50), name='list2')

    # get_correlation_plot(x1, y1)